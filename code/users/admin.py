"""User admin classes"""
# Django
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin

# Models
from users.models import Profile

# Register your models here.
# admin.site.register(Profile)

@admin.register(Profile)
class ProfiledAdmin(admin.ModelAdmin):
    """Profile admin."""
    list_display = ('pk', 'user', 'phone_number', 'website', 'picture') # Se muestran los campos sin necesidad de ir al detalle
    list_display_links = ('pk', 'user') # Envía al detalle
    list_editable = ('phone_number', 'website', 'picture') # Se pueden editar sin necesidad de ir al detalle
    search_fields = (
        'user__email',
        'user__username' 
        'user__first_name', 
        'user__last_name', 
        'phone_number'
    ) # Los campos por los que deseamos buscar
    list_filter = (
        'created',
        'modified',
        'user__is_active',
        'user__is_staff'
        ) # Define los filtros de los datos

    fieldsets = (
        ('Profile', {
            'fields': (
                ('user','picture'),),
        }),
        ('Extra info',{
            'fields': (
                ('website', 'phone_number'),
                ('biography')
            )
        }),
        ('Metadata',{
            'fields': (('created', 'modified'),),
        })
    )

    readonly_fields = ('created', 'modified') # Son los campos que no se pueden modificar

# Definir un descriptor de administrador inline para el modelo de Profiel que actúa similar a un singleton
class ProfileInline(admin.StackedInline):
    """Profile in-lin admin for users"""

    model = Profile
    can_delete = False
    verbose_name_plural = 'profiles'

# Definir un nuevo administrador de usuario
class UserAdmin(BaseUserAdmin):
    """Add profile admin to base user admin"""
    inlines = (ProfileInline, )
    list_display = (
        'username',
        'email',
        'first_name',
        'last_name',
        'is_active',
        'is_staff'
    )

# Volver a registrar UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)